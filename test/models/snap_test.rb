require 'test_helper'

class ShotTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @shot = Shot.new(owner_user_id: 1, url: "http:www.example.com", latitude: 1000, longitude: 2000)
  end

  test "should be valid" do
    assert @shot.valid?
  end

  test "owner_user_id should be present" do
    @shot.owner_user_id = " "

    assert_not @shot.valid?
  end

  test "url should be present" do
    @shot.url = " "
    
    assert_not @shot.valid?
  end

end
