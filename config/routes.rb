Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  post '/shots', to: 'shots#create'
  post '/shots/all', to: 'shots#index'
  post '/shots/user', to: 'shots#shots_for_user'
  post '/shots/newest', to: 'shots#newest'
  post '/shots/top', to: 'shots#top'
  post '/shots/delete_all', to: 'shots#delete_all'
  post '/shots/like', to: 'shots#like'
  post '/shots/dislike', to: 'shots#dislike'
  post '/shots/flag_deleted', to: 'shots#flag_deleted'
  post '/shots/delete', to: 'shots#delete_shot'

  post '/users', to: 'users#create'
  post '/users/all', to: 'users#index'
  post '/users/search', to: 'users#search'
  post '/users/delete', to: 'users#delete'

  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'

  post '/likes', to: 'likes#index'

  post '/friends', to: 'friends#create'
  post '/friends/all', to: 'friends#index'
  post '/friends/requests', to: 'friends#get_friend_requests'
  post '/friends/friends_and_requests', to: 'friends#get_friends_and_requests_for_user'
  post '/friends/delete_friend_request', to: 'friends#delete_friend_request'
  post '/friends/delete', to: 'friends#delete'
  post '/friends/delete_all', to: 'friends#delete_all'
  post '/friends/accept', to: 'friends#accept_friend_request'
  post '/friends/decline', to: 'friends#decline_friend_request'

  post '/chats', to: 'chats#index'
  post '/chats/delete_all', to: 'chats#delete_all'

  post '/messages', to: 'messages#index'
  post '/messages/create', to: 'messages#create'
  get '/messages/delete_all', to: 'messages#delete_all'
  post '/messages/mark_messages_for_chat_as_viewed', to: 'messages#mark_messages_for_chat_as_viewed'

  post '/device/registerOrUpdate', to: 'device#registerOrUpdate'
  post '/device/get', to: 'device#get'
  get '/device/', to: 'device#index'
  post '/device/delete', to: 'device#delete'
  post '/device/delete_all', to: 'device#delete_all'

end
