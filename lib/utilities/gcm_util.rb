module GcmUtil

  AUTHORIZE_KEY = "AIzaSyCxvktw3iIDQPnIwcBOUGJ5JDg1T6fjbKA"

  # Sending logic
  # send_gcm_message(["abc", "cdf"])
  def send_gcm_message(title, body, reg_ids)
    # Find devices with the corresponding reg_ids
    devices = Device.where(:reg_uuid => reg_ids)
    # Construct JSON payload
    post_args = {
      :registration_ids => reg_ids,
      :data => {
        :title  => title,
        :body => body,
        :message => "foobar"
      }
    }
    # Send the request with JSON args and headers
    RestClient.post 'https://android.googleapis.com/gcm/send', post_args.to_json, 
      :Authorization => 'key=' + AUTHORIZE_KEY, :content_type => :json, :accept => :json
  end

end