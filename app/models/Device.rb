class Device < ActiveRecord::Base
  validates :user_uuid, presence: true
  validates :reg_uuid, presence: true
  validates :platform, presence: true
  validates :app_version, presence: true
  validates_uniqueness_of :user_uuid, scope: [:reg_uuid, :platform]
end
