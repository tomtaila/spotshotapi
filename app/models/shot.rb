class Shot < ActiveRecord::Base
  geocoded_by :coordinates
  
  belongs_to :user
  has_many :likes

  validates :user_uuid, presence: true
  validates :user_name, presence: true
  validates :url, presence: true
  validates :time_created, presence: true
  validates :shot_uuid, uniqueness: true

end
