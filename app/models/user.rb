class User < ActiveRecord::Base
  has_secure_password

  has_many :shots

  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :uuid, presence: true
  validates :uuid, uniqueness: true
  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :password_digest, presence: true

end
