class Message < ActiveRecord::Base

  validates :uuid, presence: true
  validates :chat_uuid, presence: true
  validates :sender_uuid, presence: true
  validates :sender_name, presence: true
  validates :recipient_uuid, presence: true
  validates :recipient_name, presence: true

  validates_uniqueness_of :uuid

end
