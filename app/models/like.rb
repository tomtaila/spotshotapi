class Like < ActiveRecord::Base
  belongs_to :shot
  belongs_to :user

  validates :like_uuid, presence: true
  validates :user_uuid, presence: true
  validates :shot_uuid, presence: true
  validates_uniqueness_of :user_uuid, scope: :shot_uuid

end
