class Friend < ActiveRecord::Base
  
  belongs_to :user

  validates :uuid, presence: true
  validates_uniqueness_of :uuid
  validates :user_uuid, presence: true
  validates :user_name, presence: true
  validates :friend_uuid, presence: true
  validates :friend_name, presence: true

  validates_uniqueness_of :user_uuid, scope: :friend_uuid

end
