class Chat < ActiveRecord::Base

  validates :user_a_uuid, presence: true
  validates :user_a_name, presence: true
  validates :user_b_uuid, presence: true
  validates :user_b_name, presence: true

  # WATCH OUT FOR EDGE CASE WHERE USERS DELETE EACH OTHER AS FRIENDS AND READD EACH OTHER, A CHAT WILL NOT
  # BE CREATED, WHEN EVER THIS IS DONE THE CHAT WITH BOTH USERS MUST ALSO BE DELTED WHEN A FRIEND RECORD IS DELETED
  validates_uniqueness_of :user_a_uuid, scope: :user_b_uuid
  validates_uniqueness_of :uuid

end
