require 'json'

class ShotsController < ApplicationController
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def create
    shotJson = params[:shot]
    shot = Shot.new(shot_uuid: shotJson[:shot_uuid], user_uuid: shotJson[:user_uuid], user_name: shotJson[:user_name], url: shotJson[:url], thumbnail_url: shotJson[:thumbnail_url], latitude: shotJson[:latitude], longitude: shotJson[:longitude], time_created: shotJson[:time_created], is_video: shotJson[:is_video], anonymous: shotJson[:anonymous], deleted: false)
    if shotJson[:score]
      shot.score = shotJson[:score]
    else
      shot.score = 0
    end
    if shot.valid? 
      response = {success: true}
      shot.save
    else
      response = {success: false}
    end

    render json: response
  end

  def index
    location = [params[:latitude], params[:longitude]]
    radius = params[:radius]
    # Gets all shots created within 24 hours and within range specified of the users location
    response = Shot.near(location, radius).where(created_at: (Time.now - 24.hours)..Time.now)

    render json: response
  end

  def shots_for_user
    # Gets all shots created by a certain user
    response = Shot.where(user_uuid: params[:user_uuid]).where(created_at: (Time.now - 24.hours)..Time.now).order(time_created: :desc)

    render json: response
  end

  def newest
    location = [params[:latitude], params[:longitude]]
    radius = params[:radius]
    # Gets all shots created within 24 hours and within range specified of the users location
    response = Shot.near(location, radius).where(created_at: (Time.now - 24.hours)..Time.now).order(time_created: :desc)

    render json: response
  end

  def top
    location = [params[:latitude], params[:longitude]]
    radius = params[:radius]
    # Gets all shots created within 24 hours and within range specified of the users location
    response = Shot.near(location, radius).where(created_at: (Time.now - 24.hours)..Time.now).order(score: :desc)

    render json: response
  end

  def delete_all
    Shot.delete_all
    response = {}
    response[:success] = true

    render json: response
  end

  def delete_shot
    shot = Shot.find_by(shot_uuid: params[:shot_uuid])
    response = {}
    if shot && shot.delete
      response[:success] = true
    else
      response[:success] = false
    end

    render json: response
  end

  def like
    like_uuid = params[:like_uuid]
    shot_uuid = params[:shot_uuid]
    user_uuid = params[:user_uuid]
    shot = Shot.find_by(shot_uuid: shot_uuid)
    like = Like.new(like_uuid: like_uuid, user_uuid: user_uuid, shot_uuid: shot_uuid)
    user = User.find_by(uuid: user_uuid)
    response = {}

    if like.valid? && like.save
      shot.increment(:score)
      user.increment(:score)
      response[:success] = shot.save && user.save
    else
      response[:success] = false
    end

    render json: response
  end

  def dislike
    like_uuid = params[:like_uuid]
    shot_uuid = params[:shot_uuid]
    user_uuid = params[:user_uuid]
    shot = Shot.find_by(shot_uuid: shot_uuid)
    like = Like.find_by(like_uuid: like_uuid)
    user = User.find_by(uuid: user_uuid)
    response = {}

    if !like.nil? && !like.destroy.nil?
      shot.decrement(:score)
      user.decrement(:score)
      response[:success] = shot.save && user.save
    else
      response[:success] = false
    end

    render json: response
  end

  def flag_deleted
    shot_uuid = params[:shot_uuid]
    shot = Shot.find_by(shot_uuid: shot_uuid)
    response = {}

    if !shot.nil?
      shot.deleted = true
      if shot.save
        response[:success] = true
      end
    end

    render json: response
  end

end
