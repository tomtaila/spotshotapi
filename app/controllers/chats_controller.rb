require 'json'

class ChatsController < ApplicationController
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def create
    chat = Chat.new(uuid: params[:uuid], user_a_uuid: params[:user_a_uuid], user_a_name: params[:user_a_name], user_b_uuid: params[:user_b_uuid], user_b_name: params[:user_b_name])

    if chat.valid?
      response = {success: true}
      chat.save
    else
      response = {success: false}
    end

    render json: response
  end

  def index
    user_uuid = params[:user_uuid]
    # response[:non_pending] = Chat.where(user_uuid: user_uuid).where(request: false).
    response = Chat.where('user_a_uuid LIKE :user_uuid OR user_b_uuid LIKE :user_uuid', user_uuid: "%#{user_uuid}%")

    render json: response
  end

  def delete_all
    Chat.delete_all
    response = {}
    response[:success] = true

    render json: response
  end

end
