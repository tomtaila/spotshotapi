class DeviceController < ApplicationController
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  AUTHORIZE_KEY = "AIzaSyCxvktw3iIDQPnIwcBOUGJ5JDg1T6fjbKA"

  def registerOrUpdate
    response = {}
    devices = Device.where(reg_uuid: params[:reg_uuid], user_uuid: params[:user_uuid], platform: params[:platform])
    if devices.count == 0
      device = Device.new(reg_uuid: params[:reg_uuid], user_uuid: params[:user_uuid], platform: params[:platform], app_version: params[:app_version])  
      device.save!
      response[:success] = true
    elsif devices.count == 1
      device = devices.first
      device.update(app_version: params[:app_version])
    else
      response[:success] = false
    end

    render json: response
  end

  # Sending logic
  # send_gcm_message(["abc", "cdf"])
  def send_gcm_message(title, body, reg_ids)
    # Find devices with the corresponding reg_ids
    devices = Device.where(:reg_id => reg_ids)
    # Construct JSON payload
    post_args = {
      :registration_ids => reg_ids,
      :data => {
        :title  => title,
        :body => body,
        :message => "foobar"
      }
    }
    # Send the request with JSON args and headers
    RestClient.post 'https://android.googleapis.com/gcm/send', post_args.to_json, 
      :Authorization => 'key=' + AUTHORIZE_KEY, :content_type => :json, :accept => :json
  end

  # gets device record by user_uuid
  def get
    response = Device.where(user_uuid: params[:user_uuid])
    render json: response
  end

  def index
    response = {}
    response = Device.all
    render json: response
  end

  def delete
    Device.where(reg_uuid: params[:reg_uuid]).delete_all
    response = {}
    render json: response
  end

  def delete_all
    Device.delete_all
    response = {}
    render json: response
  end

end
