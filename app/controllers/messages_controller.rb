require 'json'

class MessagesController < ApplicationController
  include ApplicationHelper
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def create
    message = Message.new(uuid: params[:uuid], chat_uuid: params[:chat_uuid], body: params[:body], sender_uuid: params[:sender_uuid], sender_name: params[:sender_name], recipient_uuid: params[:recipient_uuid], recipient_name: params[:recipient_name], message_type: params[:message_type], viewed: false, sender_saved: false, recipient_saved: false, time_sent: params[:time_sent])

    if message.valid?
      response = {success: true}
      message.save
      # send gcm message
      payload = {
          :message => message,
          :response_code => 4 # MESSAGE RECEIVED RESPONSE CODE #
        }
      send_gcm_message("New Shot!", params[:sender_name].capitalize + " sent you a shot!", payload, Device.where(user_uuid: params[:recipient_uuid]).map(&:reg_uuid))
    else
      response = {success: false}
    end

    render json: response
  end

  def index
    chat_uuid = params[:chat_uuid]
    response = Message.where(chat_uuid: chat_uuid)

    render json: response
  end

  def mark_messages_for_chat_as_viewed
    messages = Message.where(chat_uuid: params[:chat_uuid]).where(recipient_uuid: params[:recipient_uuid])
    messages.each do |m|
      m.update(:viewed => true)
    end
    # send gcm message
    payload = {
      :chat_uuid => params[:chat_uuid], # CHAT_UUID #
      :response_code => 5 # MESSAGES VIEWED RESPONSE CODE #
    }
    send_gcm_message("", "", payload, Device.where(user_uuid: params[:sender_uuid]).map(&:reg_uuid))

    response = {}
    response[:chat_uuid] = params[:chat_uuid]
    render json: response
  end

  def mark_as_viewed
    message_uuid = params[:uuid]
    message = Message.where(uuid: message_uuid)

    message.viewed = true

    if message.valid?
      message.save
      response = {success: true}
      response = {message: message}
    else 
      response = {success: false}
    end

    render json: response
  end

  def update_sender_saved
    message_uuid = params[:uuid]
    sender_saved = params[:sender_saved]
    message = Message.where(uuid: message_uuid)

    message.sender_saved = sender_saved

    if message.valid?
      message.save
      response = {success: true}
    else 
      response = {success: false}
    end

    render json: response
  end

  def update_recipient_saved
    message_uuid = params[:uuid]
    recipient_saved = params[:recipient_saved]
    message = Message.where(uuid: message_uuid)

    message.recipient_saved = recipient_saved

    if message.valid?
      message.save
      response = {success: true}
    else 
      response = {success: false}
    end

    render json: response
  end

  def delete_all
    Message.delete_all
    response = {}

    render json: response
  end

end
