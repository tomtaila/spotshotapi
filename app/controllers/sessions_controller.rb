class SessionsController < ApplicationController
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def create
    # authJson = params[:user]
    user = User.find_by(name: params[:name])
    
    if user
      #try to authenticate the user - if they authenticate successfully, an instance of the User model is returned
      authenticatedUser = user.authenticate(params[:password])

      if authenticatedUser 
        response = {success: true, user: authenticatedUser}
        device = Device.find_by(user_uuid: user.uuid, platform: params[:platform], app_version: params[:app_version])
        if device
          response[device: device]
        end
        #THIS IS THE MOST IMPORTANT PART. Actually log the user in by storing their ID in the session hash with the [:user_uuid] key!
        session[:user_uuid] = authenticatedUser.uuid
      else
        response = {success: false}
      end
    else
      response = {success: false}
    end
    
    render json: response
  end

  def destroy
    session[:user_uuid] = nil
  end
end
