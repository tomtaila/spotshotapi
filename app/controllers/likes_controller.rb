require 'json'

class LikesController < ApplicationController
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def index
    user_uuid = params[:user_uuid]
    # Gets all likes created by a user 24 hours and within range specified of the users location
    response = Like.where(user_uuid: user_uuid).where(created_at: (Time.now - 24.hours)..Time.now)

    render json: response
  end

end
