require 'json'

class UsersController < ApplicationController
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def create
    userJson = params[:user]
    user = User.new(name: userJson[:name], email: userJson[:email], password: userJson[:password], uuid: userJson[:uuid])

    if user.valid? 
      response = {success: true}
      user.save
    else
      response = {success: false}
    end

    render json: response
  end

  def index 
    users = User.all
    response = users

    render json: response
  end

  # This must have the user_name parameter to indicate which user we are searching for
  # If we get a searching_user_uuid then we look for friend requests using the searching_user_uuid and the users being searched uuid
  def search
    userName = params[:user_name]
    response = {}
    user = User.find_by(name: userName)
    response[:user] = user
    if response[:user]
      response[:success] = true
      if params[:searching_user_uuid]
        if Friend.where(user_uuid: user.uuid).where(friend_uuid: params[:searching_user_uuid]).where(request: true).count > 0
          response[:friend_status] = "requested" # indicates user being searched has requested the user that is searching them
        elsif Friend.where(friend_uuid: user.uuid).where(user_uuid: params[:searching_user_uuid]).where(request: true).count > 0
          response[:friend_status] = "pending" # indicates user doing the searching has requested the user being searched and is waiting for them to accept/decline
        elsif Friend.where(friend_uuid: user.uuid).where(user_uuid: params[:searching_user_uuid]).where(request: false).count > 0
          response[:friend_status] = "friends" # indicates users are already friends
        else
          response[:friend_status] = "not_friends" # indicates users are not friends and do not have any requests between one another
        end
      end
    else 
      response[:success] = false
    end

    render json: response
  end

  def delete
    user = User.find_by(uuid: params[:uuid])
    response = {}
    response[:success] = false
    if user.delete
      response[:success] = true
    end

    render json: response
  end

end
