require 'json'

class FriendsController < ApplicationController
  include ApplicationHelper
  # Since this is an api app we do not need to sanitize for CSRF attacks since they can only be performed through a browser
  skip_before_filter  :verify_authenticity_token
  # Respond only to json requests
  respond_to :json

  def create
    friend = Friend.new(uuid: params[:uuid], user_uuid: params[:user_uuid], user_name: params[:user_name], friend_uuid: params[:friend_uuid], friend_name: params[:friend_name], request: params[:request])

    if friend.valid?
      response = {success: true}
      friend.save
      if params[:request]
        # send gcm message
        payload = {
          :friend => friend,
          :response_code => 0 # FRIEND REQUEST RESPONSE CODE #
        }
        send_gcm_message("New Friend Request!", params[:user_name].capitalize + " added you!", payload, Device.where(user_uuid: params[:friend_uuid]).map(&:reg_uuid))
      end
    else
      response = {success: false}
    end

    render json: response
  end

  def index
    user_uuid = params[:user_uuid]
    requests_only = params[:request]
    if requests_only.nil?
      # friends + requests they must respond to
      response = Friend.where(user_uuid: user_uuid) + Friend.where(friend_uuid: user_uuid).where(request: true)
    elsif requests_only
      # friend requests only
      response = Friend.where(friend_uuid: user_uuid).where(request: true)
    else
      # friends only
      response = Friend.where(user_uuid: user_uuid).where(request: false)
    end
    
    render json: response
  end

  def get_friends_and_requests_for_user
    user_uuid = params[:user_uuid]
    response = Friend.where(user_uuid: user_uuid) + Friend.where(friend_uuid: user_uuid).where(request: true)
    render json: response
  end

  # Gets all the friend requests a user has
  def get_friend_requests
    user_uuid = params[:user_uuid]
    response = Friend.where(friend_uuid: user_uuid).where(request: true)

    render json: response
  end

  # Gets friend request from user A to user B
  def get_friend_request
    friend_request = Friend.where(user_uuid: params[:user_uuid]).where(friend_uuid: params[:friend_uuid]).where(request: true).first
    response = {}
    if friend_request
      response[:success] = true
      response[:friend_request] = friend_request
    else
      response[:success] = false
    end

    render json: response
  end

  # Deletes friend request from user A to user B
  def delete_friend_request
    friend = Friend.find_by(user_uuid: params[:user_uuid], friend_uuid: params[:friend_uuid], request: true)
    
    response = {}
    if friend && friend.delete
      response[:success] = true
      # send gcm message
        payload = {
          :friend => friend,
          :response_code => 3 # FRIEND CANCELLED RESPONSE CODE #
        }
        send_gcm_message("Cancelled Friend Request!", friend[:user_name].capitalize + " cancelled the friend request they sent you!", payload, Device.where(user_uuid: params[:friend_uuid]).map(&:reg_uuid))
    else
      response[:success] = false
    end

    render json: response
  end

  def delete
    friend = Friend.find_by(user_uuid: params[:user_uuid], friend_uuid: params[:friend_uuid], request: false)
    friend_two = Friend.find_by(user_uuid: params[:friend_uuid], friend_uuid: params[:user_uuid], request: false)
    # Delete chat associated with users
    chat = Chat.find_by(user_a_uuid: params[:user_uuid], user_b_uuid: params[:friend_uuid])
    if !chat
      chat = Chat.find_by(user_a_uuid: params[:friend_uuid], user_b_uuid: params[:user_uuid])
    end

    response = {}
    if friend && friend_two && chat && friend.delete && friend_two.delete && chat.delete
      response[:success] = true
        # send gcm message
        payload = {
          :friend => friend,
          :friend_two => friend_two,
          :chat => chat,
          :response_code => 2 # FRIEND DELETED RESPONSE CODE #
        }
        send_gcm_message("Friend Deleted!", friend.user_name + " deleted " + friend.friend_name, payload, Device.where(user_uuid: params[:friend_uuid]).map(&:reg_uuid))
    else
      response[:success] = false
    end

    render json: response
  end

  def delete_all
    Friend.delete_all
    response = {}
    response[:success] = true

    render json: response
  end

  def accept_friend_request
    friend = Friend.find_by(user_uuid: params[:user_uuid], friend_uuid: params[:friend_uuid])
    friend[:request] = false
    chatUuid = params[:chat_uuid]

    friend_two = Friend.new(uuid: params[:new_friend_uuid], user_uuid: friend[:friend_uuid], user_name: friend[:friend_name], friend_uuid: friend[:user_uuid], friend_name: friend[:user_name], request: false)
    chat = Chat.new(uuid: chatUuid, user_a_uuid: friend[:user_uuid], user_a_name: friend[:user_name], user_b_uuid: friend[:friend_uuid], user_b_name: friend[:friend_name])

    response = {}
    if (friend.valid? && friend_two.valid? && chat.valid?)
      response[:success] = true
      response[:friendAB] = friend
      response[:friendBA] = friend_two
      response[:chat] = chat
      friend.save
      friend_two.save
      chat.save
      # send gcm message
      payload = {
        :friend => friend,
        :friend_two => friend_two,
        :chat => chat,
        :response_code => 1 # FRIEND ACCEPTED RESPONSE CODE #
      }
      send_gcm_message(friend_two.user_name.capitalize + " added you back!", friend_two.user_name.capitalize + " added you back!", payload, Device.where(user_uuid: params[:user_uuid]).map(&:reg_uuid))
    else
      response[:success] = false
      response[:friend_valid] = friend.valid?
      response[:friend_two] = friend_two.valid?
      response[:chat_valid] = chat.valid?
    end

    render json: response
  end

  def decline_friend_request
    friend = Friend.find(params[:uuid])
    response = {}
    if friend.valid?
      response[:success] = true
      friend.delete
    else
      response[:success] = false
    end

    render json: response
  end

end
