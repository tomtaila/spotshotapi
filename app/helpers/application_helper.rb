module ApplicationHelper

  AUTHORIZE_KEY = "AIzaSyCxvktw3iIDQPnIwcBOUGJ5JDg1T6fjbKA"

  # Sending logic
  # send_gcm_message(["abc", "cdf"])
  def send_gcm_message(title, body, payload = {}, reg_ids)
    gcm = GCM.new(AUTHORIZE_KEY)

    options = {
      :data => {
        :title => title,
        :body => body,
        :payload => payload
      }

    }
    
    gcm.send(reg_ids, options)
  end

end
