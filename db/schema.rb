# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150803013115) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chats", force: :cascade do |t|
    t.string   "uuid"
    t.string   "user_a_uuid"
    t.string   "user_a_name"
    t.string   "user_b_uuid"
    t.string   "user_b_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "devices", force: :cascade do |t|
    t.string  "user_uuid"
    t.string  "reg_uuid"
    t.string  "platform"
    t.integer "app_version"
  end

  create_table "friends", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "user_uuid"
    t.string   "friend_uuid"
    t.string   "user_name"
    t.string   "friend_name"
    t.boolean  "request"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "uuid"
  end

  add_index "friends", ["user_id"], name: "index_friends_on_user_id", using: :btree

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "shot_id"
    t.string   "like_uuid"
    t.string   "shot_uuid"
    t.string   "user_uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "likes", ["user_id"], name: "index_likes_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.string   "uuid"
    t.string   "chat_uuid"
    t.string   "body"
    t.string   "sender_uuid"
    t.string   "sender_name"
    t.string   "recipient_uuid"
    t.string   "recipient_name"
    t.integer  "message_type"
    t.boolean  "viewed"
    t.boolean  "sender_saved"
    t.boolean  "recipient_saved"
    t.integer  "time_sent",       limit: 8
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "shots", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "shot_uuid"
    t.string   "user_uuid"
    t.string   "user_name"
    t.string   "url"
    t.string   "thumbnail_url"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "time_created",  limit: 8
    t.boolean  "is_video"
    t.boolean  "anonymous"
    t.integer  "score",                   default: 0,     null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "deleted",                 default: false
  end

  add_index "shots", ["user_id"], name: "index_shots_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "uuid"
    t.string   "password_digest"
    t.integer  "phone_number",    limit: 8
    t.integer  "score",                     default: 0, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

end
