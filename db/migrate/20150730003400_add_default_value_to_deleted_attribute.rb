class AddDefaultValueToDeletedAttribute < ActiveRecord::Migration
  def up
    change_column :shots, :deleted, :boolean, :default => false
  end

  def down
    change_column :shots, :deleted, :boolean, :default => nil
  end
end
