class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :uuid
      t.string :password_digest
      t.integer :phone_number, limit: 5
      t.integer :score, default: 0, null: false

      t.timestamps null: false
    end
  end
end
