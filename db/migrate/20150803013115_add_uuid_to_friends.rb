class AddUuidToFriends < ActiveRecord::Migration
  def change
    add_column :friends, :uuid, :string
  end
end
