class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.belongs_to :user, index: true
      t.string :user_uuid
      t.string :friend_uuid
      t.string :user_name
      t.string :friend_name
      t.boolean :request

      t.timestamps null: false
    end
  end
end
