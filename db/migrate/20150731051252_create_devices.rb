class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :user_uuid
      t.string :reg_uuid
      t.string :platform
      t.integer :app_version
    end
  end
end
