class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :uuid
      t.string :chat_uuid
      t.string :body
      t.string :sender_uuid
      t.string :sender_name
      t.string :recipient_uuid
      t.string :recipient_name
      t.integer :message_type
      t.boolean :viewed
      t.boolean :sender_saved
      t.boolean :recipient_saved
      t.integer :time_sent, limit: 5

      t.timestamps null: false
    end
  end
end
