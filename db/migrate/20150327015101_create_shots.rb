class CreateShots < ActiveRecord::Migration
  def change
    create_table :shots do |t|
      t.belongs_to :user, index: true
      t.string :shot_uuid
      t.string :user_uuid
      t.string :user_name
      t.string :url
      t.string :thumbnail_url
      t.float :latitude
      t.float :longitude
      t.integer :time_created, limit: 5
      t.boolean :is_video
      t.boolean :anonymous
      t.integer :score, default: 0, null: false

      t.timestamps null: false
    end
  end
end
