class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.belongs_to :user, index: true
      t.belongs_to :shot
      t.string :like_uuid
      t.string :shot_uuid
      t.string :user_uuid

      t.timestamps null: false
    end
  end
end
