class AddDeletedFlagToShots < ActiveRecord::Migration
  def change
    add_column :shots, :deleted, :boolean
  end
end
