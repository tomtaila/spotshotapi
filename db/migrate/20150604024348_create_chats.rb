class CreateChats < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.string :uuid
      t.string :user_a_uuid
      t.string :user_a_name
      t.string :user_b_uuid
      t.string :user_b_name

      t.timestamps null: false
    end
  end
end
